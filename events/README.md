# Maschinendeck Events
Hier liegen Event-Texte sowie Bilder / Thumbnails zur werbung über Discord oder Mail.  
Aktuelle Event-Texte sind im pad hier zu finden: https://pad.maschinendeck.org/p/EventTexte

## Eventtexte (Stand 08.10.2023)

### Aktiv und (Semi-)Regelmäßig
**Chaostreff**  
Jeden Dienstag-Abend haben wir unser offenes Treffen. Wenn du schon immer mal vorbeikommen und eine Mate mit uns trinken wolltest, ist der Dienstag der Abend, an dem du das tun kannst! 

**Hackerfryhstyck**  
Jeden Samstag-Morgen frühstücken wir zusammen. Jemensch kann Sachen mitbringen, was noch fehlt kaufen wir zusammen. Wenn du noch nie bei uns vorbei geschaut hast ist der Chaostreff der bessere Einstieg, falls dieser dir nicht passt kannst du aber auch gerne zu Frystyck vorbeikommen!

**Show n' Tell**  
Hast du ein cooles Projekt, Ding oder Thema das du den Anderen zeigen möchtest? Dann ist das hier das richtige Event für dich! Trag dein Event einfach in das unten angehängt Pad ein und sobald wir mehr als fünf Einreichungen haben veranstalten wir ein Show n' Tell bei den du über dein Ding ca. 10 Minuten lang reden kannst. Das Event wir auch ins Internet gestreamt. Für die Privatsphäre stellen wir entsprechende Masken zur verfügung. 
https://pad.maschinendeck.org/p/Show_n_tell

### Events

**Bitgames**  
Jedes Jahr findet in Bitburg die "Bitgames" statt; eine LAN-Party, um Kinder (und ihre Eltern) an das Thema Gaming heranzuführen. Wie jedes Jahr ist das Maschinendeck auch wieder dabei um Spiele, Licht und gute Laune mitzubringen. Hast du Lust mitzuhelfen? Dann melde dich hier an!

**Calliope für Lehrer**  
Ein speziell auf Lehrer zugeschnittener Calliope Workshop. Wir stellen kurz den Calliope vor, gehen auf die Grundlagen der Programmierung ein und stellen ein paar Projektideen und Unterrichtsmaterial vor.

### Inaktiv

**Python Abend Anfänger**  
Jeden dritten Montag im Monat ist unser Python Anfängerabend. Wenn du zusammen mit anderen Python lernen möchtest, ist dies der Abend für dich. Komm auch gerne vorbei, wenn du nicht technisch inkliniert oder Mitglied im Maschinendeck bist! 

**Python Abend Fortgeschrittene**  
Jeden ersten Montag im Monat ist unser Pythonabend für Fortgeschrittene. Wenn du schon etwas mehr Erfahrung in der Programmierung oder in der Programmierung speziell mit Python hast, ist dies der Abend für dich. Zusammen verbessern wir unsere Python Kenntnisse!

**Themenabend**  
Jeden zweiten Samstag im Monat ist unser Themenabend. Hier gibt es coole Vorträge oder Workshops, bei denen wir zusammen unser Wissen verbessern. Die Vorträge werden durch Maschinendeck, Mitglieder oder externen Referierenden gehalten. Wenn ihr einen Themenvorschlag habt oder selber ein Vortrag halten wollt, meldet euch beim Vorstand!

**Photo Treffen**  
Jeden letzten Donnerstag im Monat ist unser Phototreff. Der Phototreff bietet ein Raum für Photo interessierte Menschen. Hier kannst du deine Bilder austauschen, dir Inspiration und technische Unterstützung holen. Du brauchst kein Vorwissen, sondern nur Lust zum Bilder machen!
