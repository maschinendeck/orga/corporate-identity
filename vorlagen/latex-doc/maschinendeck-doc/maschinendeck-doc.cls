\LoadClass{article}
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{maschinendeck-doc}[Custom Class for Maschinendeck Documents]

% Load required packages
\RequirePackage{xcolor}    % For colors, used for dark mode
\RequirePackage{svg}       % For handling SVG images
\RequirePackage{graphicx}  % For fallback image handling
\RequirePackage{geometry}  % For adjusting page size and orientation
\RequirePackage{ifthen}    % For conditional logic (if statements)
\RequirePackage{fontspec}  % Fonts, Requires XeLaTex (in TextStudio Tools > Commands > XeLaTex)
\RequirePackage{fancyhdr}  % Header and Footer elements
\RequirePackage{tikz}      % For footer image
\RequirePackage{xparse}    % Argument parsing for envs
\RequirePackage{xstring}   % Required for string manipulation
\RequirePackage{graphicx}  % Required for includegraphics
\RequirePackage[export]{adjustbox} % Fixes error: no valign option when using animategraphics

% Convert svg to pdf using
% sudo apt install librsvg2-bin
% rsvg-convert -f pdf -o out.pdf in.svg

% Convert ttf to pt1 
% sudo apt install ttf2ufm
% ttf2ufm font.ttf

% ***********************
% * Design Guide Values *
% ***********************

% Distances / Sizes
\newcommand{\mddlogosize}{42mm}        % Logo size for A4 paper
\newcommand{\mddlogospacetop}{10mm}    % Logo v-distance between logo and maschinendeck text
\newcommand{\mddlogospacebtm}{32mm}    % Logo v-distance between text and bottom of logo
\newcommand{\mddlogotextsize}{84mm}    % Logo size of text
\newcommand{\mddtitlespace}{10mm}      % V-Distance between logo and title and title and author...
\newcommand{\mddtitlespacebeamer}{0mm} % V-Distance between title and author in beamer mode
\newcommand{\mddrainbowsize}{14mm}     % Height of Rainbow (1/3 of logo height)

% Colors
\definecolor{mdecklight}{HTML}{FFFFFF}
\definecolor{mdeckdark}{HTML}{231F20}
\definecolor{mdeckred}{HTML}{E62B33}
\definecolor{mdeckorange}{HTML}{F18618}
\definecolor{mdeckyellow}{HTML}{EECC42}
\definecolor{mdeckgreen}{HTML}{48AF57}
\definecolor{mdeckblue}{HTML}{2CA7DF}

% Fonts
\newfontface\montserrat[Path=./maschinendeck-doc/]{Montserrat-Bold.ttf}
\newcommand{\montserratfont}{\montserrat}
\setmainfont[
	Path=./maschinendeck-doc/, % Path to your font files
	UprightFont = *-Regular,   % Regular font
	ItalicFont  = *-Italic,    % Italic font
	BoldFont    = *-Bold,      % Bold fon
]{Roboto}

% Default options
\newif\ifdarkmode
\darkmodefalse
\newif\ifdigital
\digitalfalse
\newif\iflandscape
\landscapefalse
\newif\ifbeamer
\beamerfalse

\DeclareOption{darkmode}{\darkmodetrue}
\DeclareOption{digital}{\digitaltrue}
\DeclareOption{landscape}{\landscapetrue}
\DeclareOption{beamer}{\beamertrue}
\DeclareOption{red}{\newcommand{\titlecolor}{mdeckred}}
\DeclareOption{orange}{\newcommand{\titlecolor}{mdeckorange}}
\DeclareOption{yellow}{\newcommand{\titlecolor}{mdeckyellow}}
\DeclareOption{green}{\newcommand{\titlecolor}{mdeckgreen}}
\DeclareOption{blue}{\newcommand{\titlecolor}{mdeckblue}}

\ProcessOptions\relax

% Define basic document layout (default orientation: portrait)
\geometry{a4paper, portrait}

% Orientation handling
\iflandscape
	\geometry{landscape}
\fi

% Change page margins when in beamer mode
\ifbeamer
	\geometry{left=10mm, right=20mm, top=25mm, bottom=25mm}
\fi

% Dark mode and color handling
\ifdarkmode
	\pagecolor{mdeckdark}
	\color{mdecklight}
\else
	\pagecolor{mdecklight}
	\color{mdeckdark}
\fi

% Document type handling (for logo)
\newcommand{\doctype}[1]{%
	\ifthenelse{\equal{#1}{digital}}{%
		\digitaltrue
	}{%
		\digitalfalse
	}
}

\newcommand{\image}[2][]{%
	\ifdarkmode
	\StrBefore{#2}{.}[\imagename]
	\StrBehind{#2}{.}[\extension]
	\edef\darkfilename{\imagename dark.\extension}
	\IfFileExists{\darkfilename}{
		\includegraphics[#1]{\darkfilename}
	}{
		\includegraphics[#1]{#2}
	}
	\else
	\includegraphics[#1]{#2}
	\fi
}

\ifbeamer
	\newenvironment{slide}[1][]{
		\begin{minipage}{\textwidth}
		\thispagestyle{plain}   
		\raggedright 
		\vspace*{-18mm}  
		\hspace*{-10mm}
		\noindent\makebox[0pt][l]{\textbf{\Huge #1}}
		\par\vspace{5mm}
	}{
		\end{minipage}
		\pagebreak
	}

	\newcommand{\slidetotoc}[1]{%
		\addcontentsline{toc}{section}{#1} % Add a TOC entry for the slide
	}

	\NewDocumentEnvironment{imageslide}{ O{0.6\textwidth} O{3mm} m m }{
		\begin{slide}[#3] % The slide title (3rd argument, mandatory)
			\vfill
			\begin{minipage}[c][\textheight]{\textwidth}
				\begin{minipage}[t]{\dimexpr#1-#2\relax} % Image width minus the horizontal space
					\image[width=\linewidth,valign=t]{#4} % Image path (4th argument)
				\end{minipage}%
				\hspace{#2} % Horizontal space between minipages (2nd optional argument)
				\begin{minipage}[t]{\dimexpr\textwidth-#1-#2\relax} % Caption width calculated as (1 - image width - hspace)
				}{
				\end{minipage}
			\end{minipage}
			\vfill
		\end{slide}
	}

\fi

% Logo inclusion based on document type (SVG logos)
\newcommand{\maschinendecklogo}{%
	\vspace{\mddlogosize}
	\ifdigital
		\ifdarkmode
			\includegraphics[width=\mddlogosize]{logo_digital.pdf}
		\else
			\includegraphics[width=\mddlogosize]{logo_digital-shadow.pdf}
		\fi
	\else
		\ifdarkmode
			\includegraphics[width=\mddlogosize]{logo_print-white.pdf}
		\else
			\includegraphics[width=\mddlogosize]{logo_print.pdf}
		\fi
	\fi
	
	\vspace{\mddlogospacetop}
	\begin{centering}
		\montserratfont
		\resizebox{\mddlogotextsize}{!}{MASCHINENDECK}
	\end{centering}
	\vspace{\mddlogospacebtm}
}

\newcommand{\putimageatbottom}{%
	\begin{tikzpicture}[remember picture, overlay]
	\node[anchor=south west, xshift=-1mm, yshift=0cm] at (current page.south west) {
		\includegraphics[width=\paperwidth,height=\mddrainbowsize]{rainbow.pdf}
	};
	\end{tikzpicture}
}

% Override \maketitle to add the title block with the logo
\renewcommand{\maketitle}{%
	\begin{titlepage}
	\begin{center}
		\maschinendecklogo \\[1em]         % Logo
		{
			\Huge
			\vspace{\mddtitlespace}
			\ifdefined\titlecolor
			{
				\color{\titlecolor}
				\textbf{\@title}
			}
			\else
				\ifdarkmode
					\color{mdecklight}
				\else
					\color{mdeckdark}
				\fi
				\textbf{\@title}
			\fi
			\ifbeamer
				\vspace{\mddtitlespacebeamer}
			\else
				\vspace{\mddtitlespace}
			\fi
		} \\[2em]  % Title
		{\large \textbf{Author:} \@author} \\[0.5em]  % Author
		{\large \textbf{Date:} \@date} \\[2em]        % Date
	\end{center}
	\putimageatbottom
	\end{titlepage}
	\ifbeamer
		\fontsize{20pt}{24pt}\selectfont
	\fi
}

% Path handling for images and logos
\graphicspath{{maschinendeck-doc/}}

% Dark mode toggle
\newcommand{\darkmode}{\darkmodetrue}

% Begin document setup
\AtBeginDocument{%
	\iflandscape
	\geometry{landscape}
	\fi
}

\endinput
